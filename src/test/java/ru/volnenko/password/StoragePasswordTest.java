package ru.volnenko.password;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.password.storage.StoragePassword;

public class StoragePasswordTest {

    @Test
    public void test() {
        StoragePassword.global().withService("app-1").login("demo").password("test");
        StoragePassword.global().withService("app-2").login("test").password("1234");

        Assert.assertFalse(StoragePassword.global().findAllByService("app-1").isEmpty());
        Assert.assertFalse(StoragePassword.global().findAllByService("app-2").isEmpty());

        Assert.assertNotNull(StoragePassword.global().findOneByLogin("test"));
        Assert.assertNotNull(StoragePassword.global().get("test"));
        Assert.assertNotNull(StoragePassword.global().findOneByLogin("demo"));
        Assert.assertNotNull(StoragePassword.global().get("demo"));
    }

    @Test
    public void testProperties() {
        Assert.assertNotNull(StoragePassword.global().findOneByLogin("server"));
        Assert.assertNotNull(StoragePassword.global().findOneByLogin("mega"));
    }

}
