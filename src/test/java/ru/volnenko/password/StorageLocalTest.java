package ru.volnenko.password;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.password.storage.StorageLocal;

import java.util.UUID;

public class StorageLocalTest {

    @Test
    public void testBuilder() {
        Assert.assertNotNull(StorageLocal.builder());
    }

    @Test
    public void testPassword() {
        Assert.assertNotNull(StorageLocal.builder().withPassword());
    }

    @Test
    public void testLogin() {
        Assert.assertNotNull(StorageLocal.global()
                .withLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void testLoginPassword() {
        Assert.assertNotNull(
                StorageLocal.global().withPassword(
                        UUID.randomUUID().toString(),
                        UUID.randomUUID().toString()
                ));
    }

}
