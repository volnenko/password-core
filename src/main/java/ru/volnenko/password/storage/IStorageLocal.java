package ru.volnenko.password.storage;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IStorageLocal {

    @NotNull
    StoragePassword withPassword();

    @NotNull
    StoragePassword withLogin(@Nullable String login);

    @NotNull
    StoragePassword withPassword(@Nullable String login, @Nullable String password);

    @NotNull
    StoragePassword withId(@Nullable String id);

    @Nullable
    StoragePassword findOneByLogin(@Nullable String login);

    @NotNull
    List<StoragePassword> findAllByLogin(@Nullable String login);

    @NotNull
    List<StoragePassword> findAllByName(@Nullable String name);

    @Nullable
    StoragePassword findById(@Nullable String id);

}
