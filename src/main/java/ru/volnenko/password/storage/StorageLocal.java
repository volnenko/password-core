package ru.volnenko.password.storage;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volnenko.password.error.IdEmptyException;
import ru.volnenko.password.error.LoginEmptyException;
import ru.volnenko.password.error.ServiceEmptyException;
import ru.volnenko.password.model.Configuration;
import ru.volnenko.password.model.Password;
import ru.volnenko.password.predicate.*;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class StorageLocal implements IStorageLocal {

    @NotNull
    private static final String FILE_NAME_JSON = "/password.json";

    @NotNull
    private static final String FILE_NAME_XML = "/password.xml";

    @NotNull
    private final static StorageLocal GLOBAL = new StorageLocal(true);

    @NotNull
    private final Map<String, StoragePassword> map = new LinkedHashMap<>();

    public StorageLocal(boolean autoLoad) {
        if (autoLoad) load();
    }

    private StorageLocal() {
        this(false);
    }

    @SneakyThrows
    private void load() {
        final InputStream is = ClassLoader.class.getResourceAsStream(FILE_NAME_JSON);
        if (is != null) loadJSON(is);
    }

    @SneakyThrows
    private void loadJSON(@NotNull final InputStream is) {
        final ObjectMapper mapper = new ObjectMapper();
        final Configuration configuration = mapper.readValue(is, Configuration.class);
        final List<Password> passwords = configuration.getPasswords();
        withPasswords(passwords);
    }

    private void load(final Properties properties) {
        if (properties == null) return;
        final Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            final Object keyObject = keys.nextElement().toString();
            if (keyObject == null) continue;
            final String key = keyObject.toString();
            final Object valueObject = properties.getProperty(key);
            if (valueObject == null) continue;
            final String value = valueObject.toString();
            withPassword(key, value);
        }
    }

    @NotNull
    public static StorageLocal global() {
        return GLOBAL;
    }

    @NotNull
    public static StorageLocal builder() {
        return new StorageLocal();
    }

    private void merge(@Nullable final StoragePassword... storagePasswords) {
        if (storagePasswords == null || storagePasswords.length == 0) return;
        for (@Nullable final StoragePassword storagePassword: storagePasswords) {
            if (storagePassword == null) continue;
            merge(storagePassword);
        }
    }

    @NotNull
    private StoragePassword merge(@NotNull final StoragePassword value) {
        map.put(value.id(), value);
        return value;
    }

    private void withPasswords(@Nullable final Iterable<Password> passwords) {
        if (passwords == null) return;
        for (@Nullable final Password password: passwords) withPassword(password);
    }

    private void withPasswords(@Nullable final Password... passwords) {
        if (passwords == null) return;
        for (@Nullable final Password password: passwords) withPassword(password);
    }

    private void withPassword(@Nullable final Password password) {
        if (password == null) return;
        merge(new StoragePassword(this, password));
    }

    @NotNull
    public final StoragePassword withPassword() {
        return merge(new StoragePassword(this));
    }

    @NotNull
    public final StoragePassword withPassword(
            @Nullable final String login,
            @Nullable final String password
    ) {
        return merge(StoragePassword.builder(this).login(login).password(password));
    }

    @NotNull
    public final StoragePassword withId(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return merge(StoragePassword.builder(this).id(id));
    }

    @NotNull
    public final StoragePassword withService(@Nullable final String service) {
        if (service == null || service.isEmpty()) throw new ServiceEmptyException();
        return merge(StoragePassword.builder(this).service(service));
    }

    @NotNull
    public final StoragePassword withLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return merge(StoragePassword.builder(this).login(login));
    }

    @Nullable
    public final  StoragePassword findOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return map.values().stream()
                .filter(password -> login.equals(password.login()))
                .findFirst().orElse(null);
    }

    @Nullable
    public final String get(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        final StoragePassword storagePassword = findOneByLogin(login);
        if (storagePassword == null) return null;
        return storagePassword.password();
    }

    @Nullable
    public final StoragePassword findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return map.values().stream()
                .filter(password -> name.equals(password.name()))
                .findFirst().orElse(null);
    }

    @NotNull
    public final List<StoragePassword> findAllByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return Collections.emptyList();
        return map.values().stream()
                .filter(password -> login.equals(password.login()))
                .collect(Collectors.toList());
    }

    @NotNull
    public final List<StoragePassword> findAllByService(@Nullable final String service) {
        if (service == null || service.isEmpty()) return Collections.emptyList();
        return map.values().stream()
                .filter(password -> {
                    @Nullable final Set<String> services = password.services();
                    if (services == null) return false;
                    return services.contains(service);
                }).collect(Collectors.toList());
    }

    @Nullable
    public final StoragePassword findOneByService(@Nullable final String service) {
        if (service == null || service.isEmpty()) return null;
        return map.values().stream().filter(password -> {
            @Nullable final Set<String> services = password.services();
            if (services == null) return false;
            return services.contains(service);
        }).findFirst().orElse(null);
    }

    @NotNull
    public final List<StoragePassword> findAllByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return Collections.emptyList();
        return map.values().stream()
                .filter(password -> name.equals(password.name()))
                .collect(Collectors.toList());
    }

    @Nullable
    public final StoragePassword findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return map.get(id);
    }

    @Nullable
    public final StoragePassword findOneByPredicates(@Nullable final Iterable<Predicate> predicates) {
        if (predicates == null) return null;
        Stream<StoragePassword> stream = map.values().stream();
        for (@Nullable final Predicate predicate: predicates) {
            if (predicate == null) continue;

            if (predicate instanceof PredicateById) {
                stream = stream.filter(password -> {
                    @Nullable final String id = password.id();
                    if (id == null || id.isEmpty()) return false;
                    return id.equals(predicate.value());
                });
            }

            if (predicate instanceof PredicateByName) {
                stream = stream.filter(password -> {
                    @Nullable final String name = password.name();
                    if (name == null || name.isEmpty()) return false;
                    return name.equals(predicate.value());
                });
            }

            if (predicate instanceof PredicateByLogin) {
                stream = stream.filter(password -> {
                    @Nullable final String login = password.login();
                    if (login == null || login.isEmpty()) return false;
                    return login.equals(predicate.value());
                });
            }

            if (predicate instanceof PredicateByCategory) {
                stream = stream.filter(password -> {
                    @Nullable final Set<String> categories = password.categories();
                    if (categories == null || categories.isEmpty()) return false;
                    return categories.contains(predicate.value());
                });
            }

        }
        return stream.findFirst().orElse(null);
    }

    @NotNull
    public StorageLocal storage() {
        return this;
    }

    @NotNull
    public StorageLocal clear() {
        map.clear();
        return this;
    }

}
