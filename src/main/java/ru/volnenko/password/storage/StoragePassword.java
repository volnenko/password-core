package ru.volnenko.password.storage;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volnenko.password.error.*;
import ru.volnenko.password.model.Password;

import java.util.LinkedHashSet;
import java.util.Set;

public final class StoragePassword {

    @NotNull
    public static StorageLocal global() {
        return StorageLocal.global();
    }

    @NotNull
    public static StoragePassword builder(final StorageLocal storageLocal) {
        return new StoragePassword(storageLocal);
    }

    @NotNull
    private final Password password;

    @NotNull
    private final StorageLocal storageLocal;

    public StoragePassword(@NotNull final StorageLocal storageLocal, @NotNull final Password password) {
        this.password = password;
        this.storageLocal = storageLocal;
    }

    public StoragePassword(@NotNull final StorageLocal storageLocal) {
        this(storageLocal, new Password());
    }

    @Nullable
    public final String environment() {
        if (password.getEnvironments() == null || password.getEnvironments().isEmpty()) return null;
        return password.getEnvironments().stream().findFirst().orElse(null);
    }

    @NotNull
    public final StoragePassword environment(@Nullable final String environment) {
        if (password.getEnvironments() == null) password.setEnvironments(new LinkedHashSet<>());
        password.getEnvironments().add(environment);
        return this;
    }

    @Nullable
    public final Set<String> environments() {
        return password.getEnvironments();
    }

    @NotNull
    public final StoragePassword environments(@Nullable Iterable<String> environments) {
        if (environments == null) return this;
        for (@Nullable final String environment: environments) environment(environment);
        return this;
    }

    @NotNull
    public final StoragePassword environments(@Nullable String... environments) {
        if (environments == null) return this;
        for (@Nullable final String environment: environments) environment(environment);
        return this;
    }

    @Nullable
    public final String category() {
        if (password.getCategories() == null) return null;
        return password.getCategories().stream().findFirst().orElse(null);
    }

    @NotNull
    public final StoragePassword category(@Nullable final String category) {
        if (category == null || category.isEmpty()) return this;
        if (password.getCategories() == null) password.setCategories(new LinkedHashSet<>());
        password.getCategories().add(category);
        return this;
    }

    @Nullable
    public final Set<String> categories() {
        return password.getCategories();
    }

    @NotNull
    public final StoragePassword categories(@Nullable final Iterable<String> categories) {
        if (categories == null) return this;
        for (@Nullable final String category: categories) category(category);
        return this;
    }

    @NotNull
    public final StoragePassword categories(@Nullable final String... categories) {
        if (categories == null) return this;
        for (@Nullable final String category: categories) category(category);
        return this;
    }

    @Nullable
    public final String grantType() {
        return password.getGrantType();
    }

    @NotNull
    public final StoragePassword grantType(@Nullable final String grantType) {
        password.setGrantType(grantType);
        return this;
    }

    @Nullable
    public final String email() {
        return password.getEmail();
    }

    @NotNull
    public final StoragePassword email(@Nullable final String email) {
        password.setEmail(email);
        return this;
    }

    @Nullable
    public final String firstName() {
        return password.getFirstName();
    }

    @NotNull
    public final StoragePassword firstName(@Nullable final String firstName) {
        password.setFirstName(firstName);
        return this;
    }

    @Nullable
    public final String lastName() {
        return password.getLastName();
    }

    @NotNull
    public final StoragePassword lastName(@Nullable final String lastName) {
        password.setLastName(lastName);
        return this;
    }

    @Nullable
    public final String middleName() {
        return password.getMiddleName();
    }

    @NotNull
    public final StoragePassword middleName(@Nullable final String middleName) {
        password.setMiddleName(middleName);
        return this;
    }

    @Nullable
    public final Set<String> scopes() {
        return password.getScopes();
    }

    @Nullable
    public final String scope() {
        if (password.getScopes() == null || password.getScopes().isEmpty()) return null;
        return password.getScopes().stream().findFirst().orElse(null);
    }

    @NotNull
    public final StoragePassword scopes(@Nullable final Iterable<String> scopes) {
        if (scopes == null) return this;
        for (@Nullable final String scope: scopes) scope(scope);
        return this;
    }

    @NotNull
    public final StoragePassword scope(@Nullable final String scope) {
        if (scope == null || scope.isEmpty()) throw new ScopeEmptyException();
        if (password.getScopes() == null) password.setServices(new LinkedHashSet<>());
        password.getScopes().add(scope);
        return this;
    }

    @Nullable
    public final Set<String> roles() {
        return password.getRoles();
    }

    @Nullable
    public final String role() {
        if (password.getRoles() == null || password.getRoles().isEmpty()) return null;
        return password.getRoles().stream().findFirst().orElse(null);
    }

    @NotNull
    public StoragePassword roles(@Nullable final Iterable<String> roles) {
        if (roles == null) return this;
        for (@Nullable final String role: roles) role(role);
        return this;
    }

    @NotNull
    public StoragePassword roles(@Nullable final String... roles) {
        if (roles == null || roles.length == 0) return this;
        for (@Nullable final String role: roles) role(role);
        return this;
    }

    public StoragePassword role(@Nullable final String role) {
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        if (password.getRoles() == null) password.setRoles(new LinkedHashSet<>());
        password.getRoles().add(role);
        return this;
    }

    @Nullable
    public final String token() {
        return password.getToken();
    }

    @NotNull
    public StoragePassword token(@Nullable final String token) {
        password.setToken(token);
        return this;
    }

    @Nullable
    public final Integer port() {
        return password.getPort();
    }

    @NotNull
    public final StoragePassword port(@Nullable final Integer port) {
        password.setPort(port);
        return this;
    }

    @Nullable
    public final String name() {
        return password.getName();
    }

    @NotNull
    public StoragePassword name(@Nullable final String name) {
        password.setName(name);
        return this;
    }

    @Nullable
    public final String description() {
        return password.getDescription();
    }

    @NotNull
    public final StoragePassword description(@Nullable final String description) {
        password.setDescription(description);
        return this;
    }

    @Nullable
    public final String host() {
        return password.getHost();
    }

    @NotNull
    public StoragePassword host(@Nullable final String host) {
        password.setHost(host);
        return this;
    }

    @Nullable
    public final String url() {
        return password.getUrl();
    }

    @NotNull
    public StoragePassword url(@Nullable final String url) {
        password.setUrl(url);
        return this;
    }

    @Nullable
    public final String login() {
        return password.getLogin();
    }

    @Nullable
    public final String password() {
        return password.getPassword();
    }

    @NotNull
    public final StoragePassword id(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        password.setId(id);
        return this;
    }

    @Nullable
    public final String id() {
        return password.getId();
    }

    @NotNull
    public final StoragePassword services(@Nullable final String... services) {
        if (services == null || services.length == 0) return this;
        for (@Nullable final String service: services) service(service);
        return this;
    }

    @NotNull
    public final StoragePassword service(@Nullable final String service) {
        if (service == null || service.isEmpty()) throw new ServiceEmptyException();
        if (password.getServices() == null) password.setServices(new LinkedHashSet<>());
        password.getServices().add(service);
        return this;
    }

    @Nullable
    public final String service() {
        if (password.getServices() == null || password.getServices().isEmpty()) return null;
        return password.getServices().stream().findFirst().orElse(null);
    }

    @Nullable
    public final Set<String> services() {
        return password.getServices();
    }

    @NotNull
    public final StoragePassword login(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        password.setLogin(login);
        return this;
    }

    @NotNull
    public final StoragePassword password(@Nullable final String password) {
        if (password == null || password.isEmpty()) throw  new PasswordEmptyException();
        this.password.setPassword(password);
        return this;
    }

    @NotNull
    public static StorageLocal builder() {
        return StorageLocal.builder();
    }

    @NotNull
    public StorageLocal storage() {
        return storageLocal;
    }

}
