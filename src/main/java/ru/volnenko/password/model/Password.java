package ru.volnenko.password.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Password {

    @Nullable
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String token;

    @Nullable
    private String grantType;

    @Nullable
    private String url;

    @Nullable
    private String host;

    @Nullable
    private Integer port;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Set<String> services = new LinkedHashSet<>();

    @Nullable
    private Set<String> roles = new LinkedHashSet<>();

    @Nullable
    private Set<String> scopes = new LinkedHashSet<>();

    @Nullable
    private Set<String> categories = new LinkedHashSet<>();

    @Nullable
    private Set<String> environments = new LinkedHashSet<>();

}
