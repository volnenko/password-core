package ru.volnenko.password.predicate;

public interface Predicate {

    String value();

}
